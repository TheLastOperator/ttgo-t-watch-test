extern "C" {
    #include <string.h>
    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"

    #include "driver/i2c.h"

    #include "esp_system.h"
    #include "esp_event.h"
    #include "esp_log.h"

    #include "lwip/err.h"
    #include "lwip/sys.h"

    #include "PCF8563.h"

    #include "lvgl/lvgl.h"
    #include "lvgl_helpers.h"
}

#include "axp20x.h"
#include "apx20x_esp.h"

void iic_set_up()
{
    i2c_config_t conf = {};
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = (gpio_num_t) 21;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = (gpio_num_t) 22;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = 100000;
    esp_err_t ret = i2c_param_config(I2C_NUM_0, &conf);
    assert(ret == ESP_OK);
    ret = i2c_driver_install(I2C_NUM_0, conf.mode, 0, 0, 0);
    assert(ret == ESP_OK);
}

void rtc_task(void *pvParameter)
{

    while (true) {
        int ret;
        PCF_DateTime date = {};
        
        ret = PCF_Init(0);
        if (ret != 0) {
            printf("PCF_Init %d\n", ret);
            continue;
        }
        ret = PCF_GetDateTime(&date);
        if (ret != 0) {
            printf("PCF_GetDateTime %d\n", ret);
            continue;
        }

        printf("The time is %d-%d-%d %d:%d:%d\n ", date.year+100,date.month,date.day,date.hour,date.minute,date.second);

        vTaskDelay(5000 / portTICK_RATE_MS);

    }

    return;
}

#define TAG "APP"
#define LV_TICK_PERIOD_MS 10

/**********************
 *  STATIC PROTOTYPES
 **********************/
static void lv_tick_task(void *arg);
void guiTask(void *pvParameter);
void power_management_init();
void power_management_task(void *pvParameter);

extern "C" void app_main()
{
    // Say hello

    ESP_LOGI (TAG, "Hello");

    // Init i2c
    
    iic_set_up();

    // Init PMU

    power_management_init();
    
    // Start tasks

    xTaskCreate(&rtc_task, "rtc_task", 2048, NULL, 5, NULL);
    xTaskCreate(&power_management_task, "pwrbtn_task", 2048, NULL, 5, NULL);

    //If you want to use a task to create the graphic, you NEED to create a Pinned task
    //Otherwise there can be problem such as memory corruption and so on
    xTaskCreatePinnedToCore(guiTask, "gui", 4096*2, NULL, 0, NULL, 1);
}

static void lv_tick_task(void *arg) {
    (void) arg;

    lv_tick_inc(LV_TICK_PERIOD_MS);
}

//Creates a semaphore to handle concurrent call to lvgl stuff
//If you wish to call *any* lvgl function from other threads/tasks
//you should lock on the very same semaphore!
SemaphoreHandle_t xGuiSemaphore;

void guiTask(void *pvParameter) {
    
    (void) pvParameter;
    xGuiSemaphore = xSemaphoreCreateMutex();

    lv_init();
    
    /* Initialize SPI or I2C bus used by the drivers */
    lvgl_driver_init();

    static lv_color_t buf1[DISP_BUF_SIZE];
    static lv_color_t buf2[DISP_BUF_SIZE];
    
    static lv_disp_buf_t disp_buf;

    uint32_t size_in_px = DISP_BUF_SIZE;


    lv_disp_buf_init(&disp_buf, buf1, buf2, size_in_px);

    lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv);
    disp_drv.flush_cb = disp_driver_flush;

    disp_drv.buffer = &disp_buf;
    lv_disp_drv_register(&disp_drv);

    const esp_timer_create_args_t periodic_timer_args = {
        .callback = &lv_tick_task,
        .arg = 0,
        .dispatch_method = ESP_TIMER_TASK,
        .name = "periodic_gui"
    };
    esp_timer_handle_t periodic_timer;
    ESP_ERROR_CHECK(esp_timer_create(&periodic_timer_args, &periodic_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer, LV_TICK_PERIOD_MS * 1000));
   
    /* use a pretty small demo for monochrome displays */
    /* Get the current screen  */
    lv_obj_t * scr = lv_disp_get_scr_act(NULL);

    /*Create a Label on the currently active screen*/
    lv_obj_t * label1 =  lv_label_create(scr, NULL);

    /*Modify the Label's text*/
    lv_label_set_text(label1, "Hello\nworld!");

    /* Align the Label to the center
     * NULL means align on parent (which is the screen now)
     * 0, 0 at the end means an x, y offset after alignment*/
    lv_obj_align(label1, NULL, LV_ALIGN_CENTER, 0, 0);
    
    while (1) {
        vTaskDelay(1);
        //Try to lock the semaphore, if success, call lvgl stuff
        if (xSemaphoreTake(xGuiSemaphore, (TickType_t)10) == pdTRUE) {
            lv_task_handler();
            xSemaphoreGive(xGuiSemaphore);
        }
    }

    //A task should NEVER return
    vTaskDelete(NULL);
}

void power_management_init()
{
    //ESP_ERROR_CHECK(i2c_master_init());

    axp_irq_init();

    if (axp.begin(twi_read, twi_write)) {
        ESP_LOGE(TAG, "Error init axp20x !!!");
        while (1);
    }
    ESP_LOGI(TAG, "Success init axp20x !!!");
/*
    axp.setPowerOutPut(AXP202_DCDC3, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_DCDC3 turned on");
    axp.setPowerOutPut(AXP202_EXTEN, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_EXTEN turned on");
    axp.setPowerOutPut(AXP202_LDO2, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_LDO2 turned on");
    axp.setPowerOutPut(AXP202_LDO4, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_LDO4 turned on");
    axp.setPowerOutPut(AXP202_DCDC2, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_DCDC2 turned on");
    axp.setLDO4Voltage(AXP202_LDO4_1800MV);
    ESP_LOGI(TAG, "AXP202_LDO4 set to 1800MV");
    axp.setLDO3Voltage(3500);
    ESP_LOGI(TAG, "AXP202_LDO3 set to 3500");
    axp.setPowerOutPut(AXP202_LDO3, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_LDO3 turned on");
*/
    axp.setPowerOutPut(AXP202_DCDC3, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_DCDC3 turned on");
    axp.setPowerOutPut(AXP202_LDO2, AXP202_ON);
    ESP_LOGI(TAG, "AXP202_LDO2 turned on");

    if ( axp.isDCDC2Enable()) {
        ESP_LOGI(TAG, "DC2: %u mV", axp.getDCDC2Voltage());
    } else {
        ESP_LOGI(TAG, "DC2: DISABLE");
    }

    if (axp.isDCDC3Enable()) {
        ESP_LOGI(TAG, "DC3: %u mV", axp.getDCDC3Voltage());
    } else {
        ESP_LOGI(TAG, "DC3: DISABLE");
    }

    if (axp.isLDO2Enable()) {
        ESP_LOGI(TAG, "LDO2: %u mV", axp.getLDO2Voltage());
    } else {
        ESP_LOGI(TAG, "LDO2: DISABLE");
    }

    if (axp.isLDO3Enable()) {
        ESP_LOGI(TAG, "LDO3: %u mV", axp.getLDO3Voltage());
    } else {
        ESP_LOGI(TAG, "LDO3: DISABLE");
    }

    if (axp.isLDO4Enable()) {
        ESP_LOGI(TAG, "LDO4: %u mV", axp.getLDO4Voltage());
    } else {
        ESP_LOGI(TAG, "LDO4: DISABLE");
    }

    if (axp.isExtenEnable()) {
        ESP_LOGI(TAG, "Exten: ENABLE");
    } else {
        ESP_LOGI(TAG, "Exten: DISABLE");
    }

    //When the chip is axp192 / 173, the allowed values are 0 ~ 15,
    //corresponding to the axp1xx_charge_current_t enumeration
    // axp.setChargeControlCur(AXP1XX_CHARGE_CUR_550MA);

    //axp202 allows maximum charging current of 1800mA, minimum 300mA
    axp.setChargeControlCur(300);

    ESP_LOGI(TAG, "setChargeControlCur:%u", axp.getChargeControlCur());

    //! Enable all irq channel
    axp.enableIRQ(AXP202_ALL_IRQ, true);

    axp.clearIRQ();
}

void power_management_task(void *pvParameter) 
{
    uint32_t io_num;
    while (1) {

        if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
            if (io_num == AXP_ISR_GPIO) {
                axp.readIRQ();
                if (axp.isPEKShortPressIRQ()) {
                    ESP_LOGI(TAG, "AXP202 PEK key Click");
                }
                axp.clearIRQ();
            }
        }
        vTaskDelay(1);
    }
}